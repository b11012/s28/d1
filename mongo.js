// MongoDB Atlas?
	/*
		>> MongoDB in the cloud. It is a cloud service of MongoDB, the leading noSQL database

	*/

// Robo3T
	/*
		>> application which allows the use of GUI to manipulate MongoDB databases.
	*/

// Shell
	/*
		>> is an interface to input commands and perform CRUD operations in our database
	*/

// CRUD Operations
	// Create, Read, Update and Delete


// Create- inserting documents
	// insertOne({})
/*
	Syntax:
		db.collection.insertOne({
			fieldA: "value A",
			fieldB: "value B"
		})
*/
	db.users.insertOne({
		firstName : "Jean",
		lastName : "Smith",
		age: 24,
		contact: {
			phone : "09123456789",
			email : "js@mail.com"
		},
		courses: ["CSS", "Javascript", "ReactJS"],

		department: "none"
	})

	//insertMany({})
	db.users.insertMany(
		{
			firstName : "Stephen",
			lastName : "Hawking",
			age: 76,
			contact: {
				phone : "09123456789",
				email : "sh@mail.com"
			},
			courses: ["Python", "PHP", "ReactJS"],

			department: "none"
		},
		{
			firstName : "Neil",
			lastName : "Armstrong",
			age: 82,
			contact: {
				phone : "09123456789",
				email : "na@mail.com"
			},
			courses: ["Laravel", "Sass", "Springboot"],

			department: "none"
		}

	)

// READ- retrieval of documents
	/*
		Syntax:
			>> return all documents
			db.collection.find()

			>> return first document
			db.collection.findOne({})

			>> return ALL documents that matches a criteria in the collection
			db.collection.find({criteria: "value"})

			>> return FIRST matching document that matches the criteria
			db.collection.findOne({criteria: "value"})

			>> return the ALL documents that matches the criteria
			db.collection.find({criteriaA: "valueA", criteriaB: "valueB", ...})
	*/

	// return all users
	db.users.find()

	// return first user
	db.users.findOne({})

	// return all users with department none
	db.users.find({department: "none"})

	// return all users enrolled in reactJS
	db.users.find({courses: 'ReactJS'})

	// return first matching user document
	db.users.findOne({department: "none"})

	// return ALL that matches the multiple criteria
	db.users.find({lastName: "Armstrong", age: 82})

	// array as criteria
	db.users.find({courses: {$all: ["Python", "ReactJS", "PHP"]}})

	// object as criteria
	db.users.find({contact: {phone: "09123456789", email:"na@mail.com"}})

	// nested field using dot notation
	db.users.find({"contact.phone": "09123456789"})


// UPDATE
	/*
		>> update first document that matches the criteria

		db.collection.updateOne(
			{
				criteria: value
			}, 
			{
				$set: 
				{
					"fieldToBeUpdated" : "updatedValue"
				}
			})

			>> one-liner
			db.collection.updateOne({criteria: value}, {$set: {fieldToBeUpdated : "updatedValue"}})

			>> to update the first item
			db.collection.updateOne({}, {$set: {fieldToBeUpdated : "updatedValue"}})

			>> to update ALL items in the collection
			db.collection.updateMany({}, {$set: {fieldToBeUpdated: "updatedValue"}})

			>> to update items that matches criteria
			db.collection.updateMany({criteria: "value"}, {$set: {fieldToBeUpdated: "value"}})

	*/

	db.users.updateOne(
		{
			firstName: "Carol"
		}, 
		{
			$set: 
			{
				lastName : "Marvel"
			}
		})


	db.users.updateOne({}, 
			{
				$set: {
					emergencyContact: "father"
				}
			}

		)

	db.users.updateMany({}, 
		{
			$set: {
				emergencyContact : "mother"
			}
		}
	)

	db.users.updateMany({department: "none"}, 
		{
			$set: {
				department : "tech"
			}
		}
	)

	db.users.updateOne({}, {$rename : {"department" : "dept"}})


// DELETE
	/*
		Syntax:

			>> delete the first item that matches the criteria
			db.collection.deleteOne({criteria: "value"})

			>> delete ALL items that matches the criteria
			db.collection.deleteMany({criteria: "value"})

			>> delete the first item in the collection
			db.collection.deleteOne({})

			>> delete ALL items in the collection
			db.collection.deleteMany({})
	*/

	db.users.deleteOne({firstName: "Neil"})

	db.users.deleteOne({})

	db.users.deleteMany({lastName: "Armstrong"})

	db.users.deleteMany({})
